<?php
return [
    'adminEmail' => 'admin@example.com',
    
     'maxFileSize' => 1024 * 1024 * 2, // 2 megabites
    'storagePath' => '@frontend/web/uploads/',
    'storageUri' => '/uploads/',   // http://images.com/uploads/g4/u0/a95450fga99294.jpg
];
